//1. Сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день недели на Украинском языке

/*
let number = parseInt(prompt("Введіть число від 1 до 7"));

function day(a) {
    switch (a) {
        case 1: return "понеділок";
        case 2: return "вівторок";
        case 3: return "середа"; 
        case 4: return "четвер";
        case 5: return "п'ятниця";
        case 6: return "субота";
        case 7: return "неділя";
    }

    return "невірне число";
}

document.write(day(number));
*/

// 2. Дана строка вида 'var_text_hello'. Сделайте из него текст 'VarTextHello'.

/*
let str = 'var_text_hello';
let arr = str.split("_");

function capitalize(c) {
    return c[0].toUpperCase() + c.slice(1);
};

let text = "";

for (let i = 0; i < arr.length; i++) {
    text += capitalize(arr[i]);
};

document.write(text);
*/

// 3. Создайте функцию которая будет заполнять массив 10-ю иксами с помощью цикла.

/*
function loop() {
    let array = [];
    for (let i = 0; i < 10; i++) {
        array.push("x");
    };

    document.write(array);
};

loop();
*/

// 4. Создайте масив на 50 элементов и заполните каждый элемент его номером, не используя циклы выведите каждый нечетный элемент в параграфе, а четный в диве.

/*
const array = [];

for (let i = 0; i < 50; i++) {
    array[i] = i;
};

function ggg(element, index) {
    index % 2 === 0 ?
        document.write(`<div>${element}</div>`) :
        document.write(`<p>${element}</p>`);
};

array.filter(ggg);
*/

// 5. Если переменная a больше нуля - то в ggg запишем функцию, которая выводит один !, иначе запишем функцию, которая выводит два !

/*
let number = +prompt("Введіть число");

function func(x) {
    return x > 0 ? "!" : "!!";
}

document.write(func(number));
*/

// 6. Используя CallBack function создайте калькулятор который будет от пользователя принимать 2 числа и знак арефметической операции. При вводе не числа или при делении на 0 выводить ошибку.

/*
function calculator(callback1, callback2, callback3) {
    let x = callback1()
    if (isNaN(x)) { return "це не число" };

    let y = callback2();
    if (isNaN(y)) { return "це не число" };

    let operator = callback3();

    switch (operator) {
        case "+": return x + y;
        case "-": return x - y;
        case "*": return x * y; 
        case "/": {
            if (y == 0) {
                return "на 0 ділити не можна";
            } else {
                return x / y;
            };
        };
    };

    return "некорректний математичний оператор";
};

document.write(calculator(
    function () {
        return +prompt("Введіть перше число");
    }, function () {
        return +prompt("Введіть друге число");
    }, function () {
        return prompt(`Введіть математичну операцію "+ - * або /"`);
    })
);
*/

// 7. Функция ggg принимает 2 параметра: анонимную функцию, которая возвращает 3 и анонимную функцию, которая возвращает 4. Верните результатом функции ggg сумму 3 и 4.

/*
function ggg(callback1, callback2) {
    return callback1() + callback2();
};

document.write(ggg(function () {
        return 3;
    }, function () {
        return 4;
}));
*/

// 8. Сделайте функцию, которая считает и выводит количество своих вызовов.

/*
let count = 0;

function counter() {
    ++count;
};

for (let i = 0; i < 5; i++) {
    counter();
};

document.write(`кількість визовів функції: ${count}`);
*/

// 9. Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.

function plusOne(array) {
    for (let i = 0; i < array.length; i++) {
        array[i]++;
    }
    return array;
};

function sortUp(array) {
    array.sort((a, b) => {return a - b });
    return array;
};

function sortDown(array) {
    array.sort((a, b) => {return b - a });
    return array;
};

function map(fn, array) {
    return fn(array);
};

let arr = [1, 2, 5, -10, -100, 1000, 5123, 255, 52324, -41234];

document.write(`дан масив ${arr}`);
document.write("<br><hr>");
document.write(`додати одиницю до кожного елементу ${map(plusOne, arr)}`);
document.write("<br><hr>");
document.write(`відсортуваний за зростанням ${map(sortUp, arr)}`);
document.write("<br><hr>");
document.write(`відсортований за спаданням ${map(sortDown, arr)}`);